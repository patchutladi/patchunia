# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Project for Web and API Automation. This project uses cucumber serenity along with Junit
V 1.0

master branch is empty, head over to the api branch for api tests and web for web automation for web tests

### How do I get set up? ###

Maven, Java 8 , our favourite IDE IntelliJ,

Use below maven command to run API tests
verify -Pdev
Test results are found under target\site\serenity\index.html

Use below to run Web tests
src\test\java\CucumberTest\Runner.java and run test (Junit)

Repo owner is Pertunia
0715035097